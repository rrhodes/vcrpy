import unittest

import vcr

from app import app

default_vcr = vcr.VCR(
    serializer='yaml',
    cassette_library_dir='cassettes',
    record_mode='once',
    match_on=['uri', 'method'],
)


class TestApp(unittest.TestCase):
    @default_vcr.use_cassette(
        "tmdb_get_latest_movie",
        filter_query_parameters=["api_key"]
    )
    def test_get_latest_movie(self):
        response = app.get_latest_movie()

        self.assertIsNotNone(response)
        self.assertEqual(200, response.status_code)


if __name__ == '__main__':
    unittest.main()
