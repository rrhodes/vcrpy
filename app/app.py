import os

import requests

API_KEY = os.getenv("TMDB_API_KEY") or None
BASE_URL = "https://api.themoviedb.org/3"


def get_latest_movie():
    return requests.get(
        "{}/movie/latest?api_key={}".format(BASE_URL, API_KEY)
    )
